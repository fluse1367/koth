package eu.software4you.minecraft.paper.koth.statistics;

import eu.software4you.minecraft.paper.koth.data.Savable;
import lombok.val;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Statistics implements Savable {
    private final Map<UUID, Ply> plies = new HashMap<>();

    private Ply lastOwner;
    private Ply owner;
    private Ply firstOwner;

    public Statistics() {
    }

    public static Statistics loadFrom(ConfigurationSection sec) {
        val stats = new Statistics();

        val plies = sec.getConfigurationSection("plies");
        plies.getKeys(false).forEach(key -> {
            UUID uuid = UUID.fromString(key);
            Ply ply = Ply.loadFrom(uuid, plies.getConfigurationSection(key));
            stats.plies.put(uuid, ply);
        });

        Optional.ofNullable(sec.getString("lastOwner")).map(UUID::fromString)
                .ifPresent(uid -> stats.lastOwner = stats.plies.get(uid));

        Optional.ofNullable(sec.getString("owner")).map(UUID::fromString)
                .ifPresent(uid -> stats.owner = stats.plies.get(uid));

        Optional.ofNullable(sec.getString("firstOwner")).map(UUID::fromString)
                .ifPresent(uid -> stats.firstOwner = stats.plies.get(uid));

        return stats;
    }

    public void setOwner(Player owner) {
        this.owner = owner != null ? ply(owner) : null;
    }

    public void setOwner(Ply owner) {
        this.owner = owner;
    }

    public Optional<Ply> getCurrentOwner() {
        return Optional.ofNullable(owner);
    }

    public void reset() {
        lastOwner = null;
        plies.clear();
    }

    public Optional<Ply> getFirstOwner() {
        return Optional.ofNullable(firstOwner);
    }

    public void setFirstOwner(Player firstOwner) {
        this.firstOwner = ply(firstOwner);
    }

    public void setFirstOwner(Ply firstOwner) {
        this.firstOwner = firstOwner;
    }

    public Optional<Ply> getLongestOwner() {
        return plies.values().stream().max(Comparator.comparingLong(Ply::getTime));
    }

    public List<Ply> statsGetMost(Function<Ply, Integer> fun) {
        val li = plies.values().stream().sorted(
                (o1, o2) -> fun.apply(o2) - fun.apply(o1)
        ).collect(Collectors.toList());
        if (li.isEmpty())
            return Collections.emptyList();
        int times = li.get(0).getTimes();
        li.removeIf(p -> p.getTimes() < times);
        return li;
    }

    public List<Ply> statsGetLeast(Function<Ply, Integer> fun) {
        val li = plies.values().stream().sorted(Comparator.comparingInt(fun::apply))
                .collect(Collectors.toList());
        if (li.isEmpty())
            return Collections.emptyList();
        int times = li.get(0).getTimes();
        li.removeIf(p -> p.getTimes() < times);
        return li;
    }

    public List<Ply> getMostTimesOwners() {
        return statsGetMost(Ply::getTimes);
    }

    public List<Ply> getMostKillers() {
        return statsGetMost(Ply::getKills);
    }

    public List<Ply> getLeastDead() {
        return statsGetLeast(Ply::getDeaths);
    }

    public Optional<Ply> getLastOwner() {
        return Optional.ofNullable(lastOwner);
    }

    public void setLastOwner(Player lastOwner) {
        this.lastOwner = ply(lastOwner);
    }

    public void setLastOwner(Ply lastOwner) {
        this.lastOwner = lastOwner;
    }

    public Ply ply(Player player) {
        return ply(player.getUniqueId());
    }

    public Ply ply(UUID player) {
        if (!plies.containsKey(player)) {
            plies.put(player, new Ply(player));
        }
        return plies.get(player);
    }

    public void plyEnd() {
        plies.values().stream().filter(Ply::running).forEach(Ply::end);
    }

    @Override
    public void saveTo(ConfigurationSection sec) {
        sec.set("lastOwner", lastOwner != null ? lastOwner.getUuid().toString() : null);
        sec.set("owner", owner != null ? owner.getUuid().toString() : null);
        sec.set("firstOwner", firstOwner != null ? firstOwner.getUuid().toString() : null);

        val plies = sec.createSection("plies");
        this.plies.forEach((uuid, ply) -> ply.saveTo(plies.createSection(uuid.toString())));
    }
}
