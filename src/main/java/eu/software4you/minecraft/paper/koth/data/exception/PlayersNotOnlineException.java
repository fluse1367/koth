package eu.software4you.minecraft.paper.koth.data.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.OfflinePlayer;

import java.util.Set;

@RequiredArgsConstructor
public class PlayersNotOnlineException extends RuntimeException {
    @Getter
    private final Set<OfflinePlayer> players;
}
