package eu.software4you.minecraft.paper.koth.event;

import eu.software4you.minecraft.paper.koth.Koth;
import eu.software4you.minecraft.paper.koth.data.Savable;
import eu.software4you.minecraft.paper.koth.game.Phase;
import eu.software4you.minecraft.paper.koth.game.Point;
import eu.software4you.minecraft.paper.koth.statistics.Ply;
import eu.software4you.minecraft.paper.koth.util.Utils;
import eu.software4you.spigot.plugin.ExtendedPlugin;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.title.Title;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitTask;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.bukkit.GameMode.*;

public class Event implements Listener, Savable {
    private final ExtendedPlugin plugin;
    private final EventManager manager;
    @Getter
    private final String id;
    private final List<BukkitTask> tasks = new ArrayList<>();
    @Getter
    private final Point point;
    @Getter
    @Setter
    private int countdown = 0;
    @Getter
    private int duration = -1;
    @Getter
    @Setter
    private Location spawnPoint;
    @Getter
    @Setter
    private Location loserPoint;
    @Getter
    @Setter
    private Location winnerPoint;
    @Getter
    @Setter
    private Location lobbyPoint;
    // in seconds
    private long timeRunning;
    private long timeLeftRunning;
    // ---

    @Getter
    private boolean setAsRunning;

    public Event(ExtendedPlugin plugin, EventManager eventManager, String id, Point point) {
        this.plugin = plugin;
        this.manager = eventManager;
        this.id = id;
        this.point = point;
    }

    public static Event loadFrom(ConfigurationSection sec) {
        val plugin = Koth.getPlugin(Koth.class);
        val point = Point.loadFrom(sec.getConfigurationSection("point"));

        String id = sec.getString("id");
        int countdown = sec.getInt("countdown");
        int duration = sec.getInt("duration");

        val event = new Event(plugin, plugin.getEventManager(), id, point);
        event.countdown = countdown;
        event.duration = duration;

        // locations
        val locs = sec.getConfigurationSection("locs");
        event.spawnPoint = locs.getLocation("spawn");
        event.loserPoint = locs.getLocation("loser");
        event.winnerPoint = locs.getLocation("winner");
        event.lobbyPoint = locs.getLocation("lobby");

        // instance
        val inst = sec.getConfigurationSection("inst");

        event.timeRunning = inst.getInt("time");
        event.timeLeftRunning = inst.getInt("left");

        if (inst.getBoolean("running")) {
            plugin.getLogger().warning("Loaded event " + id + " was previously running.");
        }

        return event;
    }

    public void start() {
        validateNotRunning();

        Validate.isTrue(duration > 0, "Duration below or equal 0: " + duration);
        Validate.isTrue(countdown >= 0, "Countdown below 0: " + countdown);
        Validate.isTrue(point.getRadius() >= .5, "Radius below 0.5: " + point.getRadius());
        Validate.notNull(spawnPoint, "Spawnpoint not set");
        Validate.notNull(loserPoint, "Loserpoint not set");
        Validate.notNull(winnerPoint, "Winnerpoint not set");


        timeRunning = -countdown;
        timeLeftRunning = countdown + duration;

        resume(false);
    }

    public boolean mayResume() {
        return timeRunning < duration + countdown && timeLeftRunning > 0;
    }

    public void resume() {
        resume(true);
    }

    private void resume(boolean actualResume) {
        validateNotRunning();

        if (!mayResume()) {
            throw new IllegalStateException("Event (" + id + ") has already ended!");
        }

        setAsRunning = true;
        manager.putRunning(this);
        plugin.registerEvents(this);
        plugin.registerEvents(point);

        Runnable spawnTeleport = () -> getParticipants().forEach(all -> all.teleport(spawnPoint));

        // teleport players to spawn if not actually resuming
        if (!actualResume) {
            if (timeRunning < 0) {
                // countdown is not over yet

                // lobby teleport
                if (lobbyPoint != null) {
                    getParticipants().forEach(all -> all.teleport(lobbyPoint));
                }

                AtomicReference<BukkitTask> task = new AtomicReference<>();
                task.set(plugin.async(() -> {
                    plugin.sync(spawnTeleport);

                    // remove "myself"
                    BukkitTask me = task.get();
                    me.cancel();
                    tasks.remove(me);
                }, -timeRunning, TimeUnit.SECONDS));
                tasks.add(task.get());
            } else {
                // countdown is over, instant start
                spawnTeleport.run();
            }
        }
        // start point
        if (!point.isRunning()) {
            if (timeRunning < 0) {
                // countdown is not over yet
                AtomicReference<BukkitTask> task = new AtomicReference<>();
                task.set(plugin.async(() -> {
                    if (!point.isRunning()) {
                        if (actualResume) {
                            point.resume();
                        } else {
                            point.start();
                        }
                    }

                    // remove "myself"
                    BukkitTask me = task.get();
                    me.cancel();
                    tasks.remove(me);
                }, -timeRunning, TimeUnit.SECONDS));
                tasks.add(task.get());
            } else {
                // countdown is over, instant start
                if (actualResume) {
                    point.resume();
                } else {
                    point.start();
                }
            }
        }

        // remaining time announcements
        val sub = plugin.getLayout().sub("event.time-remaining");
        sub.section().getKeys(false).forEach(remainingStr -> {
            if (!NumberUtils.isNumber(remainingStr))
                return;
            int remaining = Integer.parseInt(remainingStr);
            val sub2 = sub.sub(remainingStr);

            if ((timeLeftRunning - countdown) > remaining) {
                tasks.add(plugin.async(() -> {
                    point.getWorld().getPlayers().forEach(p ->
                            p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_LAND, 1, .5f));
                    point.getWorld().showTitle(Title.title(
                            Component.text(sub2.string("title")),
                            Component.text(sub2.string("subtitle")),
                            Title.DEFAULT_TIMES
                    ));
                }, timeLeftRunning - remaining, TimeUnit.SECONDS));
            }
        });

        // ending second ticking sound
        if ((timeLeftRunning - countdown) > 5) {
            tasks.add(plugin.sync(() -> point.getWorld().getPlayers().forEach(p -> {
                if (timeLeftRunning > 0) {
                    p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING,
                            SoundCategory.AMBIENT, 1, timeLeftRunning > 1 ? .5f : 1.5f);
                }
            }), timeLeftRunning - 4, 1, TimeUnit.SECONDS));
        }

        // autosave task
        val as = plugin.getConf().sub("event-autosave");
        TimeUnit unit;
        switch (as.string("unit", "MINUTES").toUpperCase()) {
            case "MILLISECONDS":
                unit = TimeUnit.MILLISECONDS;
                break;
            case "SECONDS":
                unit = TimeUnit.SECONDS;
                break;
            case "HOURS":
                unit = TimeUnit.HOURS;
                break;
            default:
                unit = TimeUnit.MINUTES;
        }
        tasks.add(plugin.sync(this::save,
                as.longNum("delay", 3),
                as.longNum("period", 3),
                unit));

        // start main ticker at last, that {time} and {left} are not modified beforehand
        tasks.add(plugin.async(() -> {
            sendActionBar();
            timeRunning++;
            if (--timeLeftRunning < 0) {
                if (setAsRunning) {
                    finish();
                }
            }
        }, 0, 20));
    }

    public void finish() {
        abort(false);

        // finalize
        val stats = point.getStatistics();

        val longest = stats.getLongestOwner();
        long time = longest.map(Ply::getTime).orElse(-1L);
        long min = time > -1 ? TimeUnit.MILLISECONDS.toMinutes(time) : -1;
        long sec = time > -1 ? TimeUnit.MILLISECONDS.toSeconds(time - TimeUnit.MINUTES.toMillis(min)) : -1;

        val mostCaptured = stats.getMostTimesOwners();
        int capturedTimes = mostCaptured.isEmpty() ? -1 : mostCaptured.get(0).getTimes();
        String mostCapturedList = mostCaptured.stream().map(Ply::getName).collect(Collectors.joining(", "));

        val winner = stats.getLastOwner();

        val mostKills = stats.getMostKillers();
        int kills = mostKills.isEmpty() ? -1 : mostKills.get(0).getKills();
        String mostKillsList = mostKills.stream().map(Ply::getName).collect(Collectors.joining(", "));

        val leastDeaths = stats.getLeastDead();
        int deaths = leastDeaths.isEmpty() ? -1 : leastDeaths.get(0).getKills();
        String leastDeathsList = leastDeaths.stream().map(Ply::getName).collect(Collectors.joining(", "));

        val sub = plugin.getLayout().sub("event.finish-statistics");
        val noone = sub.string("noone");

        Component c = Component.text(
                sub.string("message",
                        /*top holder*/(Object) longest.map(p -> p.getOfflinePlayer().getName()).orElse(noone), min, sec,
                        /*most captures*/mostCaptured.isEmpty() ? noone : mostCapturedList, capturedTimes,
                        /*most kills*/mostKillsList.isEmpty() ? noone : mostKillsList, kills,
                        /*least deaths*/leastDeaths.isEmpty() ? noone : leastDeathsList, deaths,
                        /*last captured*/winner.map(Ply::getName).orElse(noone),
                        /*first captured*/stats.getFirstOwner().map(Ply::getName).orElse(noone),
                        /*current captured*/stats.getCurrentOwner().map(Ply::getName).orElse(noone)
                )
        );

        point.getWorld().sendMessage(c);

        // ending sound
        point.getWorld().getPlayers().forEach(p -> p.playSound(p.getLocation(),
                Sound.UI_TOAST_CHALLENGE_COMPLETE, SoundCategory.AMBIENT, 1, 1));

        // teleport & announce winnner
        winner.ifPresent(w -> {
            val sub2 = plugin.getLayout().sub("event.winner-announcement");
            point.getWorld().showTitle(Title.title(Component.text(sub2.string("title", (Object) w.getName())),
                    Component.text(sub2.string("subtitle")),
                    Title.Times.of(Duration.ofMillis(500), Duration.ofSeconds(3), Duration.ofSeconds(1))
            ));
            w.getPlayer().ifPresent(player -> plugin.sync(() -> player.teleport(winnerPoint)));
        });

        // teleport losers
        val losers = getParticipants().stream()
                .filter(p -> p != winner.flatMap(Ply::getPlayer).orElse(null))
                .collect(Collectors.toList());
        plugin.sync(() -> losers.forEach(p -> {
            p.teleport(loserPoint);
            if (p.getGameMode() == SPECTATOR)
                p.setGameMode(SURVIVAL);
        }));
    }

    public void abort() {
        abort(true);
    }

    private void abort(boolean resumeIntended) {
        validateRunning();
        setAsRunning = false;
        manager.unputRunning(this);
        plugin.unregisterEvents(this);
        plugin.unregisterEvents(point);

        tasks.forEach(BukkitTask::cancel);
        tasks.clear();

        if (resumeIntended)
            save(); // saving BEFORE point stop

        if (point.isRunning())
            point.stop(resumeIntended);

        if (!resumeIntended) {
            timeLeftRunning = 0;
            timeRunning = duration + countdown;
            save();
        }
    }

    private void sendActionBar() {

        val sec = plugin.getLayout().section("event.countdown");

        String actionBar;
        if (timeRunning < 0) {
            actionBar = Utils.formatHMS(sec.getString("start-in"), Math.abs(timeRunning));
        } else if (timeLeftRunning > 0) {
            actionBar = Utils.formatHMS(sec.getString("end-in"), this.timeLeftRunning);
        } else {
            actionBar = sec.getString("end");
        }

        point.getWorld().sendActionBar(Component.text(actionBar));
    }

    private void validateNotRunning() {
        if (setAsRunning)
            throw new IllegalStateException("Already set as running");
        if (manager.isRunning(getWorld()))
            throw new IllegalStateException("Another event is already registered as running in the world " + getWorld().getName());
    }

    private void validateRunning() {
        if (!setAsRunning)
            throw new IllegalStateException("Not set as running");
        if (!manager.isRunning(this))
            throw new IllegalStateException("This event (" + id + ") is not registered as running in the world " + getWorld().getName());
    }

    @EventHandler
    public void handle(PlayerDeathEvent e) {
        val p = e.getEntity();
        if (p.getWorld() != getWorld())
            return;

        e.setCancelled(true);
        e.setShouldPlayDeathSound(true);

        if (timeRunning < 0) {
            if (lobbyPoint != null) {
                p.teleport(lobbyPoint);
            }
            return;
        }

        val killer = Optional.ofNullable(p.getKiller());

        val sub = plugin.getLayout().sub("event");

        p.setGameMode(SPECTATOR);
        p.setMetadata("dead", new FixedMetadataValue(plugin, true));
        p.showTitle(Title.title(Component.text(sub.string("death.title")),
                Component.text(killer.map(pl -> sub.string("death.subtitle", (Object) pl.getName())).orElse(""))
        ));

        point.getStatistics().ply(p).death();

        killer.ifPresent(pl -> {
            point.getStatistics().ply(pl).kill();
            pl.sendMessage(sub.string("kill", (Object) p.getName()));
            pl.playSound(pl.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, .5f, 1);
        });

        if (timeLeftRunning > 5) {
            plugin.sync(() -> {
                p.setHealth(Optional.ofNullable(p.getAttribute(Attribute.GENERIC_MAX_HEALTH))
                        .map(AttributeInstance::getValue).orElse(20D));
                p.setFoodLevel(20);
                p.setSaturation(5);
                p.teleport(spawnPoint);
                p.setGameMode(SURVIVAL);
                p.removeMetadata("dead", plugin);
            }, 5 * 20);
        }
    }

    @EventHandler
    public void handle(PlayerJoinEvent e) {
        val p = e.getPlayer();
        if (timeRunning < 0) {
            if (lobbyPoint != null) {
                plugin.sync(() -> {
                    if (p.getWorld() == getWorld()) {
                        p.teleport(lobbyPoint);
                    }
                }, 5);
            }
            return;
        }

        plugin.sync(() -> {
            if (p.getWorld() == getWorld()) {
                p.teleport(spawnPoint);
            }
        }, 5);
    }

    @EventHandler
    public void handle(PlayerQuitEvent e) {
        if (timeRunning < 0) {
            return;
        }

        val p = e.getPlayer();
        if (point.isOwner(p)) {
            point.setOwner(null);
            point.setShield(0);
            point.setProgress(0);
            point.setPhase(Phase.Independent);
            point.getBar().removePlayer(p);
            point.getShieldBar().removePlayer(p);
        }
    }

    @Override
    public void saveTo(ConfigurationSection sec) {
        sec.set("id", id);

        point.saveTo(sec.createSection("point"));

        sec.set("countdown", countdown);
        sec.set("duration", duration);

        // locations
        val locs = sec.createSection("locs");
        locs.set("spawn", spawnPoint);
        locs.set("loser", loserPoint);
        locs.set("winner", winnerPoint);
        locs.set("lobby", lobbyPoint);

        // instance
        val inst = sec.createSection("inst");
        inst.set("running", setAsRunning);
        inst.set("time", timeRunning);
        inst.set("left", timeLeftRunning);
    }

    public void save() {
        manager.saveEvent(this);
    }

    public World getWorld() {
        return point.getWorld();
    }

    public List<Player> getParticipants() {
        return getWorld().getPlayers().stream().filter(p -> p.getGameMode() != CREATIVE
                && (p.hasMetadata("dead") || p.getGameMode() != SPECTATOR))
                .collect(Collectors.toList());
    }

    public boolean isRunning() {
        return setAsRunning && manager.isRunning(this);
    }

    public void setDuration(int duration) {
        int oldDuration = this.duration;
        this.duration = duration;

        if (setAsRunning) {

            int diff = oldDuration - duration;
            long newTimeLeftRunning = timeLeftRunning - diff;

            if (newTimeLeftRunning <= 0) {
                this.timeLeftRunning = 0;
                finish();
            } else {
                abort(true);
                this.timeLeftRunning = newTimeLeftRunning;
                resume();
            }

        }
    }
}
