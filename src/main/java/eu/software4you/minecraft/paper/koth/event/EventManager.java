package eu.software4you.minecraft.paper.koth.event;

import eu.software4you.minecraft.paper.koth.Koth;
import lombok.val;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.*;

public class EventManager {
    private final Koth plugin;
    private final File eventsDir;
    private final Map<String, Event> events = new HashMap<>();
    private final Map<World, Event> running = new HashMap<>();

    public EventManager(Koth plugin) {
        this.plugin = plugin;
        this.eventsDir = new File(plugin.getDataFolder(), "events");
        if (!eventsDir.exists()) {
            if (!eventsDir.mkdirs()) {
                throw new IllegalStateException("Could not create " + eventsDir);
            }
        }
    }

    public void reload() {
        getRunning().forEach(Event::abort);
        events.clear();
        running.clear();

        for (File file : eventsDir.listFiles(file -> file.isFile() && file.getName().toLowerCase().endsWith(".yml"))) {
            // not using #addEvent to prevent save() (which would overwrite the point instance as it's not being load at this time)
            val event = Event.loadFrom(YamlConfiguration.loadConfiguration(file));
            verifyNotRegistered(event.getId());
            events.put(event.getId(), event);
        }
    }

    public boolean isRegistered(String id) {
        return events.containsKey(id);
    }

    public boolean isRunning(Event event) {
        verifyRegistered(event);
        return running.get(event.getWorld()) == event;
    }

    public boolean isRunning(World world) {
        return running.containsKey(world);
    }

    public Set<Event> getRunning() {
        return Collections.unmodifiableSet(new HashSet<>(running.values()));
    }

    public void addEvent(Event event) {
        verifyNotRegistered(event.getId());
        events.put(event.getId(), event);
        event.save();
    }

    public boolean delEvent(Event event) {
        verifyRegistered(event);
        if (event.isSetAsRunning())
            throw new IllegalStateException("Event (" + event.getId() + ") cannot be deleted: It is marked as running.");
        events.remove(event.getId());
        return new File(eventsDir, event.getId() + ".yml").delete();
    }

    public Optional<Event> getEvent(String id) {
        if (!isRegistered(id))
            return Optional.empty();
        return Optional.of(events.get(id));
    }

    public Set<String> getRegisteredIds() {
        return Collections.unmodifiableSet(events.keySet());
    }

    public boolean tryLoadPointInstance(Event event) {
        verifyRegistered(event);
        File file = new File(eventsDir, event.getId() + ".yml");
        if (!file.exists()) {
            plugin.getLogger().warning("Event (" + event.getId() + ") point instance could not load: no event file");
            return false;
        }
        val sec = YamlConfiguration.loadConfiguration(file).getConfigurationSection("point.inst");
        if (sec == null) {
            plugin.getLogger().warning("Event (" + event.getId() + ") point instance could not load: malformed configuration");
            return false;
        }
        event.getPoint().loadInstance(sec);
        return true;
    }

    void putRunning(Event event) {
        verifyRegistered(event);
        if (isRunning(event.getWorld()))
            throw new IllegalStateException("Another event is already running in the world" + event.getWorld().getName());
        running.put(event.getWorld(), event);
    }

    void unputRunning(Event event) {
        verifyRegistered(event);
        if (!isRunning(event))
            throw new IllegalStateException("This event is not running in the world " + event.getWorld().getName());
        running.remove(event.getWorld(), event);
    }

    synchronized void saveEvent(Event event) {
        verifyRegistered(event.getId());
        events.get(event.getId()).saveToFile(new File(eventsDir, event.getId() + ".yml"));
    }

    private void verifyRegistered(Event event) {
        verifyRegistered(event.getId());
    }

    private void verifyRegistered(String id) {
        if (!isRegistered(id))
            throw new IllegalArgumentException("The event id " + id + " is not registered!");
    }

    private void verifyNotRegistered(String id) {
        if (isRegistered(id))
            throw new IllegalArgumentException("The event id " + id + " is already registered!");
    }
}
