package eu.software4you.minecraft.paper.koth.statistics;

import eu.software4you.minecraft.paper.koth.Koth;
import eu.software4you.minecraft.paper.koth.data.Savable;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

/**
 * Player statistic container.
 */
@Getter
public class Ply implements Savable {
    private final UUID uuid;
    @Getter(AccessLevel.NONE)
    private long start;

    private long time;

    private int times;
    private int kills;
    private int deaths;

    public Ply(Player player) {
        this.uuid = player.getUniqueId();
    }

    public Ply(UUID uuid) {
        this.uuid = uuid;
    }

    private Ply(UUID uuid, ConfigurationSection sec) {
        this(uuid);

        time = sec.getLong("time");
        times = sec.getInt("times");
        kills = sec.getInt("kills");
        deaths = sec.getInt("deaths");

        if (sec.getBoolean("running")) {
            start = System.currentTimeMillis();
            Koth.getPlugin(Koth.class).getLogger().warning("Ply " + uuid + " was previously running.");
        }
    }

    public static Ply loadFrom(UUID uuid, ConfigurationSection sec) {
        return new Ply(uuid, sec);
    }

    public Optional<Player> getPlayer() {
        return Optional.ofNullable(Bukkit.getPlayer(uuid));
    }

    public OfflinePlayer getOfflinePlayer() {
        return Bukkit.getOfflinePlayer(uuid);
    }

    public String getName() {
        return getOfflinePlayer().getName();
    }

    public void start() {
        if (running())
            throw new IllegalStateException("Cannot start if already running");
        start = System.currentTimeMillis();
        times++;
    }

    public void end() {
        if (!running())
            throw new IllegalStateException("Cannot stop if not running");
        this.time += System.currentTimeMillis() - start;
        start = 0;
    }

    public void kill() {
        kills++;
    }

    public void death() {
        deaths++;
    }

    /**
     * Updates the time without ending measurement.
     */
    public void updateTime() {
        if (!running())
            throw new IllegalStateException("Cannot update time if not currently measuring.");

        this.time += System.currentTimeMillis() - start;
        start = System.currentTimeMillis();
    }

    public boolean running() {
        return start > 0;
    }

    @Override
    public void saveTo(ConfigurationSection sec) {
        if (running())
            updateTime();
        sec.set("time", time);
        sec.set("times", times);
        sec.set("running", running());
        sec.set("kills", kills);
        sec.set("deaths", deaths);
    }
}
