package eu.software4you.minecraft.paper.koth.game;

public enum Phase {
    /**
     * When the point is not owned and a player is within it
     */
    Capturing,

    /**
     * When the point was in the process ob being captured but not anymore
     */
    UnCapturing("un-capturing"),

    /**
     * When a player and the owner are within the point
     */
    Defending,

    /**
     * When the point is not owned and 2 (or more) players are within it
     */
    Embattled,

    /**
     * When the point is owned and only the owner is within it
     */
    Captured,

    /**
     * When the point is not owned and nobody is within it
     */
    Independent,

    /**
     * When the point is owned and the owner is not within it
     */
    Shield_Draining,

    /**
     * When the point is owned, the owner is within it but the shield energy is not 100%
     */
    Shield_Recovering,

    ;

    public final String layoutId;

    Phase(String layoutId) {
        this.layoutId = layoutId != null ? layoutId : name().toLowerCase();
    }

    Phase() {
        this(null);
    }
}
