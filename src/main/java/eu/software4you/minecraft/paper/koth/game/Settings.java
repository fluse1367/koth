package eu.software4you.minecraft.paper.koth.game;

import eu.software4you.minecraft.paper.koth.data.Savable;
import org.bukkit.configuration.ConfigurationSection;

import static eu.software4you.minecraft.paper.koth.util.Utils.div;

public class Settings implements Savable {
    final long tickPeriod;

    final double incr;
    final double decr;
    final double decrEnemy;

    final double shieldIncrRecover;
    final double shieldDecrDrain;
    final double shieldDecrDrainEnemy;

    private final long capturingTime;
    private final long unCapturingTime;
    private final long unCapturingEnemyTime;

    private final long shieldDrainTime;
    private final long shieldDrainEnemyTime;
    private final long shieldRecoverTime;

    /**
     * @param capturingTime        Time (in ticks) that it takes to (fully) capture the point.
     * @param unCapturingTime      Time (in ticks) that it takes to (fully) lose the point when in capturing progress.
     * @param unCapturingEnemyTime Same as {@code unCapturingTime} but when enemies are within the point.
     * @param shieldDrainTime      Time (in ticks) that it takes to (fully) drain the shield.
     * @param shieldDrainEnemyTime Same as {@code shieldDrainTime} but when enemies are within the point.
     * @param shieldRecoverTime    Time (in ticks) that it takes to (fully) recover the shield.
     * @param tickPeriod           Period (in ticks) that the main calculation will be repeated.
     */
    public Settings(long capturingTime, long unCapturingTime, long unCapturingEnemyTime, long shieldDrainTime, long shieldDrainEnemyTime, long shieldRecoverTime, long tickPeriod) {
        this.capturingTime = capturingTime;
        this.unCapturingTime = unCapturingTime;
        this.unCapturingEnemyTime = unCapturingEnemyTime;

        this.shieldDrainTime = shieldDrainTime;
        this.shieldDrainEnemyTime = shieldDrainEnemyTime;
        this.shieldRecoverTime = shieldRecoverTime;

        this.tickPeriod = tickPeriod;

        incr = div(this.tickPeriod, this.capturingTime);
        decr = div(this.tickPeriod, this.unCapturingTime);
        decrEnemy = div(this.tickPeriod, this.unCapturingEnemyTime);

        shieldIncrRecover = div(this.tickPeriod, this.shieldRecoverTime);
        shieldDecrDrain = div(this.tickPeriod, this.shieldDrainTime);
        shieldDecrDrainEnemy = div(this.tickPeriod, this.shieldDrainEnemyTime);
    }

    public Settings() {
        this(
                20 * 30,
                20 * 15,
                20 * 8,
                20 * 15,
                20 * 8,
                20 * 5,
                2
        );
    }

    public static Settings loadFrom(ConfigurationSection sec) {
        return new Settings(
                sec.getLong("capturingTime"),
                sec.getLong("unCapturingTime"),
                sec.getLong("unCapturingEnemyTime"),
                sec.getLong("shieldDrainTime"),
                sec.getLong("shieldDrainEnemyTime"),
                sec.getLong("shieldRecoverTime"),
                sec.getLong("tickPeriod")
        );
    }

    @Override
    public void saveTo(ConfigurationSection sec) {
        sec.set("capturingTime", capturingTime);
        sec.set("unCapturingTime", unCapturingTime);
        sec.set("unCapturingEnemyTime", unCapturingEnemyTime);
        sec.set("shieldDrainTime", shieldDrainTime);
        sec.set("shieldDrainEnemyTime", shieldDrainEnemyTime);
        sec.set("shieldRecoverTime", shieldRecoverTime);
        sec.set("tickPeriod", tickPeriod);
    }
}
