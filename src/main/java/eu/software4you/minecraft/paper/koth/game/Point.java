package eu.software4you.minecraft.paper.koth.game;

import eu.software4you.minecraft.paper.koth.Koth;
import eu.software4you.minecraft.paper.koth.data.Savable;
import eu.software4you.minecraft.paper.koth.data.exception.PlayersNotOnlineException;
import eu.software4you.minecraft.paper.koth.statistics.Statistics;
import eu.software4you.minecraft.paper.koth.util.Utils;
import eu.software4you.reflect.Parameter;
import eu.software4you.reflect.ReflectUtil;
import eu.software4you.spigot.plugin.ExtendedPlugin;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.*;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static eu.software4you.minecraft.paper.koth.game.Phase.*;
import static org.bukkit.GameMode.CREATIVE;
import static org.bukkit.GameMode.SPECTATOR;

@Getter
@RequiredArgsConstructor
public class Point implements Savable, Listener {
    @NotNull
    private final ExtendedPlugin plugin;
    @NotNull
    private final BossBar bar = Bukkit.createBossBar("§c?", BarColor.WHITE, BarStyle.SOLID, BarFlag.DARKEN_SKY, BarFlag.CREATE_FOG);
    @NotNull
    private final BossBar shieldBar = Bukkit.createBossBar("§c?", BarColor.PINK, BarStyle.SOLID);
    @NotNull
    private final Settings settings;
    // caching
    private final List<Player> players = new ArrayList<>();
    @Getter(AccessLevel.NONE)
    private final List<Player> rivals = new ArrayList<>();
    @Getter(AccessLevel.NONE)
    private final List<Entity> entities = new ArrayList<>();
    @NotNull
    private Location location;
    private double radius = -1;
    private double progress;
    private double shield;
    @NotNull
    private Phase phase = Independent;
    @Nullable
    private Player owner;
    @Nullable
    private Player capturing;
    // internal stuff
    @Getter(AccessLevel.NONE)
    private BukkitTask ticker;
    @Getter(AccessLevel.NONE)
    private BukkitTask cacheTicker;
    @Getter(AccessLevel.NONE)
    private BukkitTask animationTask;
    private boolean running;
    @Getter(AccessLevel.NONE)
    private boolean intruders;

    private Statistics statistics = new Statistics();

    public static Point loadFrom(ConfigurationSection sec) {
        val plugin = Koth.getPlugin(Koth.class);
        val settings = Settings.loadFrom(sec.getConfigurationSection("settings"));
        val location = sec.getLocation("location");

        val point = new Point(plugin, settings, location);
        point.radius = sec.getDouble("radius");

        return point;
    }

    public void start() {
        if (running) {
            throw new IllegalStateException();
        }

        this.owner = null;
        this.capturing = null;

        statistics.reset();

        setPhase(Phase.Independent);

        setProgress(0);
        setShield(0);

        resume();
    }

    public void resume() {
        if (running) {
            throw new IllegalStateException();
        }
        running = true;

        shieldBar.setTitle(plugin.getLayout().string("event.point.shield"));

        // tickers
        cacheTicker = plugin.sync(this::refreshCaches, 0, settings.tickPeriod);
        ticker = plugin.async(this::tick, 10, settings.tickPeriod);

        restartAnimation();
    }

    private void restartAnimation() {
        if (!isRunning())
            return;

        if (animationTask != null) {
            animationTask.cancel();
        }

        // floating block
        Entity floatingBlock;
        entities.add(floatingBlock = getWorld().spawnEntity(getLocation().clone().add(0, radius - 1, 0), EntityType.ARMOR_STAND,
                CreatureSpawnEvent.SpawnReason.CUSTOM, ent -> {
                    ArmorStand as = (ArmorStand) ent;
                    as.setGravity(false);
                    as.setInvisible(true);
                    as.setInvulnerable(true);
                    as.setCollidable(false);
                    as.setCanMove(false);
                    as.setCanTick(false);

                    // ((CraftEntity) as).getHandle().noclip = false;
                    ReflectUtil.call(as.getClass(), as, "getHandle().noclip",
                            null, Parameter.guessMultiple(false));
                    as.setRotation(0, 0);

                    String mat = plugin.getLayout().string("event.point.floating-block." + getPhase().layoutId);
                    as.getEquipment().setHelmet(new ItemStack(Material.valueOf(mat)));
                }));


        // "charging" particle animation
        List<List<Location>> coords = new ArrayList<>();
        for (double r = radius; r > 0; r -= .1) {
            int a = (int) (32 * radius);
            if (a <= 0)
                continue;
            coords.add(Utils.circle(r, a, location));
        }

        float s = .75f;
        val red = new Particle.DustOptions(Color.RED, s);
        val orange = new Particle.DustOptions(Color.ORANGE, s);
        val blue = new Particle.DustOptions(Color.AQUA, s);
        AtomicInteger ai = new AtomicInteger(0);


        AtomicInteger floatingBlockDelta = new AtomicInteger(0);
        animationTask = plugin.async(() -> {
            // "charging" pulse particle animation
            if (!getPlayers().isEmpty()) {
                // render pulse
                int i;
                if ((i = ai.getAndAdd(1)) >= coords.size() - 1) {
                    // i is at the last available index, reset to 0
                    ai.set(0);
                }
                coords.get(i).forEach(loc ->
                        loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 1, red));
            }
            // (always render edge)
            coords.get(0).forEach(loc -> loc.getWorld().spawnParticle(Particle.REDSTONE, loc, 1,
                    progress > 0 ? (players.isEmpty() ? orange : red) : blue));
            // ---

            // floating block animation
            int delta = floatingBlockDelta.addAndGet(5);
            if (delta >= 360) {
                floatingBlockDelta.set(0);
            }
            double locDelta = Math.sin(Math.toRadians(delta));
            floatingBlock.teleport(getLocation().clone().add(0, (radius - 1) + locDelta / 8d, 0));
            floatingBlock.setRotation(delta, 0);

        }, 0, 1);
    }

    @EventHandler
    public void handle(PlayerInteractAtEntityEvent e) {
        if (entities.contains(e.getRightClicked())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void handle(EntityDamageEvent e) {
        if (entities.contains(e.getEntity())) {
            e.setCancelled(true);
        }
    }

    void refreshCaches() {
        // refresh caches
        val playersUpdated = location.getNearbyPlayers(radius).stream()
                .filter(p -> p.getGameMode() != CREATIVE)
                .filter(p -> p.hasMetadata("dead") || p.getGameMode() != SPECTATOR)
                .filter(p -> p.getLocation().distance(location) <= radius)
                .collect(Collectors.toList());
        Utils.updateList(playersUpdated, players);

        val rivalsUpdated = players.stream().filter(p -> !isOwner(p) && p != capturing)
                .collect(Collectors.toList());
        Utils.updateList(rivalsUpdated, rivals);

        intruders = players.stream().anyMatch(p -> !isOwner(p));
        // ---
    }

    private void tick() {
        // update players receiving the boss bars
        Utils.updateBarPlayers(bar, getWorld().getPlayers());

        if (shield <= 0) {
            shieldBar.removeAll();
        } else {
            Utils.updateBarPlayers(shieldBar, getWorld().getPlayers());
        }
        // ---

        // tick - actual game logic
        switch (this.phase) {
            case Independent:
                if (!players.isEmpty()) {
                    if (players.size() == 1) {
                        capturing = players.get(0);
                        setPhase(Capturing);
                    } else {
                        setPhase(Embattled);
                    }
                }
                break;
            case Captured:
                if (!isOwnerGuarding()) {
                    setPhase(Shield_Draining);
                    setShield(1);
                } else if (hasIntruders()) {
                    setPhase(Defending);
                }
                break;
            case Embattled:
                if (owner != null) {
                    if (hasIntruders()) {
                        if (isOwnerGuarding()) {
                            setPhase(Defending);
                        }
                    } else if (isOwnerGuarding()) {
                        setShield(1);
                        setPhase(Captured);
                    } else {
                        setPhase(Shield_Draining);
                    }
                } else if (capturing != null) {
                    if (players.contains(capturing)) {
                        if (rivals.isEmpty()) {
                            setPhase(Capturing);
                        }
                    } else {
                        setPhase(UnCapturing);
                    }
                } else if (players.isEmpty()) {
                    setProgress(0);
                    setPhase(Independent);
                } else if (players.size() == 1) {
                    capturing = players.get(0);
                    setPhase(Capturing);
                }
                break;
            case Shield_Draining:
                if (isOwnerGuarding()) {
                    if (hasIntruders()) {
                        setPhase(Defending);
                    } else {
                        setPhase(Shield_Recovering);
                    }
                } else {
                    setShield(getShield() - (hasIntruders() ? settings.shieldDecrDrainEnemy : settings.shieldDecrDrain));
                    if (getShield() <= 0) {
                        setOwner(null);
                        setProgress(0d);
                        setPhase(Independent);
                        if (!players.isEmpty()) {
                            // implicit hasIntruders = true
                            if (players.size() >= 2) {
                                setPhase(Embattled);
                            } else {
                                // implicit only 1 player (intruder)
                                capturing = players.get(0);
                                setPhase(Capturing);
                            }
                        } else {
                            setPhase(Independent);
                        }
                    }
                }
                break;
            case UnCapturing:
                if (players.contains(capturing)) {
                    if (!rivals.isEmpty()) {
                        setPhase(Embattled);
                    } else {
                        setPhase(Capturing);
                    }
                } else {
                    setProgress(getProgress() - (rivals.isEmpty() ? settings.decr : settings.decrEnemy));
                    if (getProgress() <= 0) {
                        capturing = null;

                        if (!players.isEmpty()) {
                            if (players.size() == 1) {
                                capturing = players.get(0);
                                setPhase(Capturing);
                            } else {
                                setPhase(Embattled);
                            }
                        } else {
                            setPhase(Independent);
                        }
                    }
                }
                break;
            case Shield_Recovering:
                if (isOwnerGuarding()) {
                    if (hasIntruders()) {
                        setPhase(Defending);
                    } else {
                        setShield(getShield() + settings.shieldIncrRecover);
                        if (getShield() >= 1) {
                            setPhase(Captured);
                        }
                    }
                } else {
                    setPhase(Shield_Draining);
                }
                break;
            case Defending:
                if (isOwnerGuarding()) {
                    if (!hasIntruders()) {
                        if (shield < 1) {
                            setPhase(Shield_Recovering);
                        } else {
                            setPhase(Captured);
                        }
                    }
                } else {
                    setPhase(Shield_Draining);
                }
                break;
            case Capturing:
                if (players.contains(capturing)) {
                    if (rivals.isEmpty()) {
                        setProgress(getProgress() + settings.incr);
                        if (getProgress() >= 1) {
                            setOwner(capturing);
                            capturing = null;
                            setShield(1);
                            Utils.updateBarPlayers(shieldBar, getWorld().getPlayers());
                            setPhase(Captured);
                        }
                    } else {
                        setPhase(Embattled);
                    }
                } else {
                    setPhase(UnCapturing);
                }

                break;
        }
        // ---

    }

    public void stop(boolean resumeIntended) {
        if (!running) {
            throw new IllegalStateException();
        }
        running = false;

        ticker.cancel();
        ticker = null;
        cacheTicker.cancel();
        cacheTicker = null;
        animationTask.cancel();
        animationTask = null;

        entities.forEach(Entity::remove);
        entities.clear();


        if (!resumeIntended) {
            statistics.plyEnd();

            setProgress(0);
            setShield(0);
            this.owner = null; // NOT using #setOwner(Player) on purpose
            capturing = null;
            setPhase(Phase.Independent);
        }

        getBar().removeAll();
        getShieldBar().removeAll();
    }

    public void setOwner(@Nullable Player owner) {
        if (this.owner == owner)
            return;

        statistics.setOwner(owner);
        Player preOwner = this.owner;
        this.owner = owner;

        if (owner != null) { // also happens if preOwner and owner are NOT null
            statistics.ply(owner).start();
            statistics.setLastOwner(owner);
            if (!statistics.getFirstOwner().isPresent()) {
                statistics.setFirstOwner(owner);
            }
        }

        if (preOwner != null) {
            statistics.ply(preOwner).end();

            getWorld().getPlayers().forEach(p -> {
                p.playSound(p.getLocation(), Sound.ENTITY_WITHER_DEATH, SoundCategory.AMBIENT, .5f, 1.5f);
                val sub = plugin.getLayout().sub("event.point.lost");
                p.sendTitle(sub.string("title", (Object) preOwner.getName()), sub.string("subtitle"), 10, 40, 20);
            });
        } else if (owner != null) {
            getWorld().getPlayers().forEach(p -> {
                p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, SoundCategory.AMBIENT, .5f, 2f);
                val sub = plugin.getLayout().sub("event.point.captured");
                p.sendTitle(sub.string("title", (Object) owner.getName()), sub.string("subtitle"), 10, 40, 20);
            });

            plugin.sync(() -> {
                Location loc = location.clone();
                int y = getWorld().getMaxHeight();
                do {
                    if (--y < 0) {
                        loc.setY(location.getY());
                        break;
                    }
                    loc.setY(y);
                } while (loc.getBlock().getType() == Material.AIR);


                Firework firework = (Firework) getWorld().spawnEntity(loc.add(0, 1, 0), EntityType.FIREWORK);
                val meta = firework.getFireworkMeta();
                meta.addEffect(FireworkEffect.builder()
                        .withFlicker()
                        .withColor(Color.RED)
                        .with(FireworkEffect.Type.BALL_LARGE)
                        .build());
                meta.setPower(1);
                firework.setFireworkMeta(meta);
            });
        }
    }

    public void setShield(double shield) {
        this.shield = Utils.range(0, 1, shield);
        shieldBar.setProgress(this.shield);
    }

    public void setProgress(double progress) {
        this.progress = Utils.range(0, 1, progress);
        bar.setProgress(this.progress);
    }

    public void setPhase(Phase phase) {
        val sub = plugin.getLayout().sub("event.point.bar");
        switch (phase) {
            case Capturing:
                if (capturing == null) {
                    throw new IllegalStateException();
                }
                bar.setTitle(sub.string("capturing", (Object) capturing.getName()));
                bar.setColor(BarColor.BLUE);
                break;
            case UnCapturing:
                if (capturing == null) {
                    throw new IllegalStateException();
                }
                bar.setTitle(sub.string("un-capturing", (Object) capturing.getName()));
                bar.setColor(BarColor.YELLOW);
                break;
            case Defending:
                // TODO: ???
                //bar.setTitle("§cUmkämpft");
                bar.setColor(BarColor.YELLOW);
                break;
            case Embattled:
                bar.setTitle(sub.string("Embattled"));
                bar.setColor(BarColor.WHITE);
                break;

            case Captured:
                if (owner == null) {
                    throw new IllegalStateException();
                }
                bar.setTitle(sub.string("captured", (Object) owner.getName()));
                bar.setColor(BarColor.BLUE);
                shieldBar.setColor(BarColor.PURPLE);
                break;
            case Independent:
                if (owner != null || capturing != null) {
                    throw new IllegalStateException();
                }
                bar.setTitle(sub.string("independent"));
                bar.setColor(BarColor.WHITE);
                break;

            case Shield_Draining:
                if (owner == null) {
                    throw new IllegalStateException();
                }
                shieldBar.setColor(BarColor.RED);
                break;
            case Shield_Recovering:
                if (owner == null || capturing != null) {
                    throw new IllegalStateException();
                }
                shieldBar.setColor(BarColor.BLUE);
                break;
        }

        String layID = phase.layoutId;

        val fbSec = plugin.getLayout().section("event.point.floating-block");
        if (entities.size() > 0 && fbSec.isSet(layID) && fbSec.isString(layID)) {
            val is = new ItemStack(Material.valueOf(fbSec.getString(layID)));
            ((ArmorStand) entities.get(0)).getEquipment().setHelmet(is);
        }

        this.phase = phase;
    }

    public void setLocation(Location location) {
        this.location = location;
        restartAnimation();
    }

    public void setRadius(double radius) {
        this.radius = radius;
        restartAnimation();
    }

    @NotNull
    public World getWorld() {
        return location.getWorld();
    }

    public boolean isOwnerGuarding() {
        return owner != null && players.contains(owner);
    }

    public boolean isOwner(Player player) {
        return player != null && owner != null && player == owner;
    }

    public boolean hasIntruders() {
        return this.intruders;
    }

    @Override
    public void saveTo(ConfigurationSection sec) {
        settings.saveTo(sec.createSection("settings"));

        sec.set("location", location);
        sec.set("radius", radius);

        // instance
        val inst = sec.createSection("inst");
        inst.set("running", running);
        statistics.saveTo(inst.createSection("stats"));
        inst.set("phase", phase.name());
        inst.set("owner", owner != null ? owner.getUniqueId().toString() : null);
        inst.set("capturing", capturing != null ? capturing.getUniqueId().toString() : null);
        inst.set("progress", progress);
        inst.set("shield", shield);
    }

    public void loadInstance(ConfigurationSection inst) {
        Set<OfflinePlayer> offline = new HashSet<>();

        val oUID = Optional.ofNullable(inst.getString("owner"))
                .map(UUID::fromString);
        owner = oUID.map(Bukkit::getPlayer).orElse(null);
        if (oUID.isPresent() && owner == null) {
            offline.add(Bukkit.getOfflinePlayer(oUID.get()));
        }

        val cUID = Optional.ofNullable(inst.getString("capturing"))
                .map(UUID::fromString);
        capturing = cUID.map(Bukkit::getPlayer).orElse(null);
        if (cUID.isPresent() && capturing == null) {
            offline.add(Bukkit.getOfflinePlayer(cUID.get()));
        }

        if (!offline.isEmpty())
            throw new PlayersNotOnlineException(offline);

        statistics = Statistics.loadFrom(inst.getConfigurationSection("stats"));

        setPhase(Phase.valueOf(inst.getString("phase")));
        setProgress(inst.getDouble("progress"));
        setShield(inst.getDouble("shield"));


        if (inst.getBoolean("running")) {
            plugin.getLogger().warning("Loaded point was previously running.");
        }
    }
}
