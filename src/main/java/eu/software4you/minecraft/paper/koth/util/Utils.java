package eu.software4you.minecraft.paper.koth.util;

import eu.software4you.math.MathUtils;
import org.bukkit.Location;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {

    /**
     * @param a lower bound (inclusive)
     * @param b upper bound (inclusive)
     * @param d value
     */
    public static double range(double a, double b, double d) {
        return Math.max(a, Math.min(b, d));
    }

    public static void updateBarPlayers(BossBar bar, Collection<Player> players) {
        sync(players, bar.getPlayers(), bar::addPlayer, bar::removePlayer);
    }

    public static <T> void sync(Collection<T> source, Collection<T> target, Consumer<T> add, Consumer<T> remove) {
        // add elements from source to target
        source.forEach(t -> {
            if (!target.contains(t)) {
                add.accept(t);
            }
        });

        // remove elements that do not exists in source from target
        target.forEach(t -> {
            if (!source.contains(t)) {
                remove.accept(t);
            }
        });
    }

    public static double div(double a, double b) {
        return BigDecimal.valueOf(a)
                .divide(BigDecimal.valueOf(b), new MathContext(4, RoundingMode.DOWN))
                .doubleValue();
    }

    public static List<Location> circle(double radius, int amount, Location center) {
        return MathUtils.getCircleCoordinates(radius, amount)
                .stream()
                .map(c -> center.clone().add(c.getX(), 0, c.getY()))
                .collect(Collectors.toList());
    }

    public static <T> void updateList(List<T> source, List<T> target) {
        target.removeIf(e -> !source.contains(e));
        source.forEach(e -> {
            if (!target.contains(e)) {
                target.add(e);
            }
        });
    }


    /**
     * @param time the time to convert in seconds
     */
    public static String formatHMS(String format, long time) {
        long hours = TimeUnit.SECONDS.toHours(time);
        time -= TimeUnit.HOURS.toSeconds(hours);
        long minutes = TimeUnit.SECONDS.toMinutes(time);
        long seconds = time - TimeUnit.MINUTES.toSeconds(minutes);

        return String.format(format, hours, minutes, seconds);
    }

    public static List<String> filter(String str, List<String> strs) {
        return filter(str, strs.stream());
    }

    public static List<String> filter(String str, String... strs) {
        return filter(str, Stream.of(strs));
    }

    public static List<String> filter(String str, Stream<String> stream) {
        return stream.filter(s -> s.startsWith(str)).collect(Collectors.toList());
    }
}
