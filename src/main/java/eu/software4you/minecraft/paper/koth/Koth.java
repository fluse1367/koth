package eu.software4you.minecraft.paper.koth;

import eu.software4you.minecraft.paper.koth.data.exception.PlayersNotOnlineException;
import eu.software4you.minecraft.paper.koth.event.Event;
import eu.software4you.minecraft.paper.koth.event.EventManager;
import eu.software4you.minecraft.paper.koth.game.Point;
import eu.software4you.minecraft.paper.koth.game.Settings;
import eu.software4you.minecraft.paper.koth.util.Utils;
import eu.software4you.spigot.plugin.ExtendedJavaPlugin;
import lombok.Getter;
import lombok.val;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

public class Koth extends ExtendedJavaPlugin {

    @Getter
    private EventManager eventManager;

    @Override
    public void onEnable() {
        reloadConfigAndLayout();

        eventManager = new EventManager(this);
        eventManager.reload();

        val c = getCommand("kingofthehill");

        c.setExecutor((sender, command, label, args) -> {
            if (command.getName().equalsIgnoreCase("kingofthehill")) {
                if (args.length == 0) {
                    return false;
                }
                if ((args[0].equalsIgnoreCase("create") || args.length >= 2 && args[1].equalsIgnoreCase("set"))
                        && !(sender instanceof Player)) {
                    getLayout().sendString(sender, "error.not-a-player");
                    return true;
                }

                switch (args[0].toLowerCase()) {
                    case "help":
                        getLayout().sendString(sender, "help");
                        return true;
                    case "reload":
                        reloadConfigAndLayout();
                        eventManager.reload();
                        getLayout().sendString(sender, "reloaded");
                        return true;
                    case "create":
                        if (args.length != 2) {
                            return false;
                        }
                        String id = args[1].toLowerCase();
                        if (id.equals("help") || id.equals("create") || id.equals("reload")) {
                            getLayout().sendString(sender, "error.reserved", (Object) id);
                            return true;
                        }
                        if (eventManager.isRegistered(id)) {
                            getLayout().sendString(sender, "error.already.id");
                            return true;
                        }

                        val point = new Point(this, new Settings(), ((Player) sender).getLocation());
                        Event event = new Event(this, eventManager, id, point);

                        eventManager.addEvent(event);

                        getLayout().sendString(sender, "created", (Object) id);
                        return true;
                }

                if (args.length >= 2) {
                    String id = args[0].toLowerCase();

                    val opEv = eventManager.getEvent(id);
                    if (!opEv.isPresent()) {
                        getLayout().sendString(sender, "error.no.id", (Object) id);
                        return true;
                    }
                    Event event = opEv.get();

                    String action = args[1].toLowerCase();

                    switch (action) {
                        case "start":
                        case "resume":
                            if (eventManager.isRunning(event.getWorld())) {
                                if (event.isRunning()) {
                                    getLayout().sendString(sender, "error.already.running",
                                            (Object) id);
                                } else {
                                    getLayout().sendString(sender, "error.already.running-another",
                                            (Object) id, event.getWorld().getName());
                                }
                                return true;
                            }

                            val pre = getLayout().sub("error.preconditions");
                            // check preconditions
                            if (!(event.getDuration() > 0)) {
                                pre.sendString(sender, "duration", (Object) id);
                                return true;
                            }
                            if (!(event.getPoint().getRadius() >= .5)) {
                                pre.sendString(sender, "radius", (Object) id);
                                return true;
                            }
                            if (event.getSpawnPoint() == null) {
                                pre.sendString(sender, "spawnpoint", (Object) id);
                                return true;
                            }
                            if (event.getWinnerPoint() == null) {
                                pre.sendString(sender, "winnerpoint", (Object) id);
                                return true;
                            }
                            if (event.getLoserPoint() == null) {
                                pre.sendString(sender, "loserpoint", (Object) id);
                                return true;
                            }


                            if (action.equals("start")) {
                                event.start();
                                getLayout().sendString(sender, "started", (Object) id);
                            } else {
                                try {
                                    if (!event.mayResume()) {
                                        getLayout().sendString(sender, "resume.fail.ended", (Object) id);
                                    } else if (eventManager.tryLoadPointInstance(event)) {
                                        event.resume();
                                        getLayout().sendString(sender, "resume.success", (Object) id);
                                    } else {
                                        getLayout().sendString(sender, "resume.fail.unknown", (Object) id);
                                    }
                                } catch (PlayersNotOnlineException e) {
                                    String players = e.getPlayers().stream().map(OfflinePlayer::getName)
                                            .collect(Collectors.joining(", "));

                                    getLayout().sendString(sender, "resume.fail.players-not-online",
                                            (Object) id, players);
                                }
                            }
                            return true;
                        case "fastfinish":
                        case "abort":
                            if (!event.isRunning()) {
                                getLayout().sendString(sender, "error.no.running", (Object) id);
                                return true;
                            }
                            if (action.equals("fastfinish")) {
                                event.finish();
                                getLayout().sendString(sender, "finished", (Object) id);
                            } else {
                                event.abort();
                                getLayout().sendString(sender, "aborted", (Object) id);
                            }
                            return true;
                    }

                    if (args.length >= 3 && action.equals("set")) {
                        String what = args[2].toLowerCase();
                        switch (what) {
                            case "point":
                            case "spawnpoint":
                            case "winnerpoint":
                            case "loserpoint":
                            case "lobbypoint":
                                switch (what) {
                                    case "point":
                                        val point = event.getPoint();
                                        point.setLocation(((Player) sender).getLocation());
                                        break;
                                    case "spawnpoint":
                                        event.setSpawnPoint(((Player) sender).getLocation());
                                        break;
                                    case "winnerpoint":
                                        event.setWinnerPoint(((Player) sender).getLocation());
                                        break;
                                    case "loserpoint":
                                        event.setLoserPoint(((Player) sender).getLocation());
                                        break;
                                    case "lobbypoint":
                                        event.setLobbyPoint(((Player) sender).getLocation());
                                        break;
                                }
                                event.save();
                                getLayout().sendString(sender, "set." + what, (Object) id);
                                return true;
                            case "countdown":
                            case "radius":
                            case "duration":
                                if (args.length < 4)
                                    return false;
                                String to = args[3].toLowerCase();

                                try {
                                    Object o;
                                    if (what.equals("radius")) {
                                        double r = Math.max(.5, Math.abs(Double.parseDouble(to)));
                                        o = r;
                                        val point = event.getPoint();
                                        point.setRadius(r);
                                    } else {
                                        int i = Math.abs(Integer.parseInt(to));
                                        o = i;
                                        if (what.equalsIgnoreCase("countdown")) {
                                            event.setCountdown(i);
                                        } else {
                                            event.setDuration(Math.max(1, i));
                                        }
                                    }
                                    event.save();
                                    getLayout().sendString(sender, "set." + what, (Object) id, o);
                                } catch (NumberFormatException e) {
                                    getLayout().sendString(sender, "error.number", (Object) to);
                                }
                                return true;
                        }
                    }
                }

            }
            return false;
        });

        c.setTabCompleter((sender, command, alias, args) -> {
            String id = args.length > 0 ? args[0].toLowerCase() : "";

            if (args.length == 0 || args.length == 1) {
                List<String> li = new ArrayList<>(Arrays.asList("help", "reload", "create"));
                li.addAll(eventManager.getRegisteredIds());
                return Utils.filter(id, li);
            } else if (eventManager.isRegistered(id)) {
                String action = args[1].toLowerCase();
                switch (args.length) {
                    case 2:
                        return Utils.filter(action, "set", "start", "abort", "fastfinish", "resume");
                    case 3:
                        if (action.equals("set"))
                            return Utils.filter(args[2].toLowerCase(),
                                    "point", "spawnpoint", "winnerpoint", "loserpoint", "lobbypoint",
                                    "radius", "countdown", "duration");
                }

            }

            return Collections.emptyList();
        });

    }

    private void reloadConfigAndLayout() {
        reloadConfig();
        setLayoutLocale(getConf().string("lang", "en").equalsIgnoreCase("de") ? Locale.GERMAN : null);
        reloadLayout();
    }

    @Override
    public void onDisable() {
        eventManager.getRunning().forEach(Event::abort);
    }
}
