package eu.software4you.minecraft.paper.koth.data;

import lombok.SneakyThrows;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public interface Savable {
    void saveTo(ConfigurationSection sec);

    @SneakyThrows
    default void saveToFile(File file) {
        YamlConfiguration yaml = new YamlConfiguration();
        saveTo(yaml);
        yaml.save(file);
    }
}
